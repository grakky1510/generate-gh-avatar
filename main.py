from os import environ
from fastapi import FastAPI
from strawberry.fastapi import GraphQLRouter
from schema import schema
from fastapi_sqlalchemy import DBSessionMiddleware

app = FastAPI()

graphql_app = GraphQLRouter(schema)


app.include_router(graphql_app, prefix="/api")

DB_USER = environ.get("DB_USER", "postgre")
DB_PASSWORD = environ.get("DB_PASSWORD", "postgre")
DB_HOST = environ.get("DB_HOST", "localhost")
DB_NAME = environ.get("DB_NAME", "dbname")

SQLALCHEMY_DATABASE_URL = (
    f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:5432/{DB_NAME}"
)
app.add_middleware(DBSessionMiddleware, db_url=SQLALCHEMY_DATABASE_URL)
