import datetime
from os import environ
from uuid import uuid4

import sqlalchemy
from sqlalchemy import Column, create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

DB_USER = environ.get("DB_USER", "postgres_acc")
DB_PASSWORD = environ.get("DB_PASSWORD", "postgres_acc")
DB_HOST = environ.get("DB_HOST", "localhost")
DB_NAME = environ.get("DB_NAME", "postgres_acc")

SQLALCHEMY_DATABASE_URL = (
    f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:5432/{DB_NAME}"
)
engine = create_engine(SQLALCHEMY_DATABASE_URL, echo=False)
Base = declarative_base()
Session = sessionmaker(bind=engine)


class UsersPictures(Base):
    __tablename__ = "auth_user"
    picture = Column(sqlalchemy.String(30), nullable=False)
