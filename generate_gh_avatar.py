import random
import uuid
import warnings

import numpy as np

from neuro_model import *

import torch.backends.cudnn as cudnn
from matplotlib import pyplot as plt

cudnn.benchmark = True

from PIL import Image

warnings.filterwarnings("ignore", category=UserWarning)
from util import display_image, load_model, post_process_generator_output


def generate_gh_image():
    device = "cuda"  # @param ['cuda', 'cpu']

    generator = Generator(256, 512, 8, channel_multiplier=2).eval().to(device)

    mean_latent = load_model(generator, "gh.pt")
    truncation = 0.5

    gh_seed = random.randint(0, 100000)

    plt.rcParams["figure.dpi"] = 150
    with torch.no_grad():
        torch.manual_seed(gh_seed)
        reference_code = torch.randn([1, 512]).to(device)
        latent = generator.get_latent(
            reference_code, truncation=truncation, mean_latent=mean_latent
        )
        reference_im, _ = generator(latent)
        gh_image = display_image(reference_im, size=None, title="GH Image")
        gh_image = post_process_generator_output(gh_image)
        uuid4 = uuid.uuid4()
        filepath = "avatars/" + str(uuid4) + ".jpg"
        Image.fromarray(np.uint8(gh_image), "RGB").save(filepath)
    return filepath
