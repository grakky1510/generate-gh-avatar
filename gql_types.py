import typing
import strawberry
import datetime

from strawberry.file_uploads import Upload


@strawberry.input
class RequestAvatar:
    set_as_avatar: typing.Optional[bool] = False


@strawberry.input
class RequestDisneyAvatar:
    set_as_avatar: typing.Optional[bool] = False
    use_avatar: typing.Optional[bool] = False
    file: Upload = None



@strawberry.type
class GeneratedAvatar:
    avatar: str


@strawberry.type
class ToonifiedAvatarOutput:
    #uuid: str
    image: str
