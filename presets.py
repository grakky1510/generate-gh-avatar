from dataclasses import dataclass, field
from typing import Dict


@dataclass
class PresetsCollection:

    action_bad_token: Dict[str, bool] = field(
        default_factory=lambda: {
            "success": False,
            "message": "Bad token",
        }
    )
