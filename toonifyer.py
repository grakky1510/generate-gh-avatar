import random
import uuid
import warnings
import torch.backends.cudnn as cudnn
import numpy as np
from matplotlib import pyplot as plt

cudnn.benchmark = True
from neuro_model import *
from PIL import Image, ImageEnhance

warnings.filterwarnings(
    "ignore", category=UserWarning
)  # get rid of interpolation warning
from util import display_image, load_model, post_process_generator_output, load_source


def toonifier(filename):
    device = "cuda"  # @param ['cuda', 'cpu']

    generator = Generator(256, 512, 8, channel_multiplier=2).eval().to(device)
    generator2 = Generator(256, 512, 8, channel_multiplier=2).eval().to(device)

    mean_latent1 = load_model(generator, "face.pt")
    mean_latent2 = load_model(generator2, "disney.pt")
    truncation = 0.5

    disney_seed = random.randint(1, 10000)  # @param {type:"number"}
    plt.rcParams["figure.dpi"] = 150

    with torch.no_grad():
        source_code = torch.randn([1, 512]).to(device)
        latent1 = generator.get_latent(
            source_code, truncation=truncation, mean_latent=mean_latent1
        )
        source = load_source([filename], generator, device)
        source_im, _ = generator(source)
        torch.manual_seed(disney_seed)
        reference_code = torch.randn([1, 512]).to(device)
        latent2 = generator2.get_latent(
            reference_code, truncation=truncation, mean_latent=mean_latent2
        )

    num_swap = 6
    alpha = 0.5

    early_alpha = 0

    with torch.no_grad():
        noise1 = [
            getattr(generator.noises, f"noise_{i}") for i in range(generator.num_layers)
        ]
        noise2 = [
            getattr(generator2.noises, f"noise_{i}")
            for i in range(generator2.num_layers)
        ]

        out1 = generator.input(source[0])
        out2 = generator2.input(latent2[0])
        out = (1 - early_alpha) * out1 + early_alpha * out2

        out1, _ = generator.conv1(out, source[0], noise=noise1[0])
        out2, _ = generator2.conv1(out, latent2[0], noise=noise2[0])
        out = (1 - early_alpha) * out1 + early_alpha * out2
        #     out = out2

        skip1 = generator.to_rgb1(out, source[1])
        skip2 = generator2.to_rgb1(out, latent2[1])
        skip = (1 - early_alpha) * skip1 + early_alpha * skip2

        i = 2
        for (
            conv1_1,
            conv1_2,
            noise1_1,
            noise1_2,
            to_rgb1,
            conv2_1,
            conv2_2,
            noise2_1,
            noise2_2,
            to_rgb2,
        ) in zip(
            generator.convs[::2],
            generator.convs[1::2],
            noise1[1::2],
            noise1[2::2],
            generator.to_rgbs,
            generator2.convs[::2],
            generator2.convs[1::2],
            noise2[1::2],
            noise2[2::2],
            generator2.to_rgbs,
        ):
            conv_alpha = early_alpha if i < num_swap else alpha
            out1, _ = conv1_1(out, source[i], noise=noise1_1)
            out2, _ = conv2_1(out, latent2[i], noise=noise2_1)
            out = (1 - conv_alpha) * out1 + conv_alpha * out2
            i += 1

            conv_alpha = early_alpha if i < num_swap else alpha
            out1, _ = conv1_2(out, source[i], noise=noise1_2)
            out2, _ = conv2_2(out, latent2[i], noise=noise2_2)
            out = (1 - conv_alpha) * out1 + conv_alpha * out2
            i += 1

            conv_alpha = early_alpha if i < num_swap else alpha
            skip1 = to_rgb1(out, source[i], skip)
            skip2 = to_rgb2(out, latent2[i], skip)
            skip = (1 - conv_alpha) * skip1 + conv_alpha * skip2

            i += 1
        image = skip.clamp(-1, 1)
        image = display_image(image)
        image = post_process_generator_output(image)
        image = Image.fromarray(np.uint8(image), "RGB")
        enhancer = ImageEnhance.Brightness(image).enhance(1.6)
        uuid4 = uuid.uuid4()
        filepath = "toonified_avatars/" + str(uuid4) + ".jpg"
        enhancer.save(filepath)
    return filepath

