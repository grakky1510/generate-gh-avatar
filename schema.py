import re
import typing

import jwt
from datetime import datetime, timedelta

from strawberry.file_uploads import Upload

from generate_gh_avatar import generate_gh_image
import strawberry
from passlib.context import CryptContext
from fastapi_sqlalchemy import db

from gql_types import (
    RequestAvatar,
    GeneratedAvatar, ToonifiedAvatarOutput, RequestDisneyAvatar,
)
from projector import projector
from toonifyer import toonifier


@strawberry.type
class Mutation:
    @strawberry.mutation
    def get_gh_avatar(self, info, get_gh_avatar: RequestAvatar) -> GeneratedAvatar:
        file = generate_gh_image()
        # TODO: saving image as user avatar
        """if get_gh_avatar.set_as_avatar:
            username = verify_token(info=info)
            if type(username) != str:
                raise Exception("Bad token")
            user = db.session.query(User).filter_by(username=username).one_or_none()
            user.avatar = file
            db.session.commit()
            db.session.close()"""
        return GeneratedAvatar(avatar=file)

    @strawberry.mutation
    async def toonify_avatar(
            self, info, set_as_avatar: typing.Optional[bool] = False, use_avatar: typing.Optional[bool] = False,
            file: Upload = None
    ) -> ToonifiedAvatarOutput:
        # TODO: saving image as user avatar
        """username = verify_token(info=info)
        if type(username) != str:
            raise Exception("Bad token")
        user = db.session.query(User).filter_by(username=username).one_or_none()
        if get_toonified_avatar.use_avatar:
            file = user.avatar
        elif info.context.FILES.get("avatar"):
            file = info.context.FILES.get("avatar")
        else:
            raise Exception("No file")"""
        if file:
            filename = file.filename
            with open(filename, "wb") as new_avatar:
                new_avatar.write(await file.read())
            with open(filename, 'rb') as image:
                projected_avatar = projector(image)
                toonified_avatar = toonifier(projected_avatar)
            return ToonifiedAvatarOutput(image=toonified_avatar)
        """
        if get_toonified_avatar.set_as_avatar:
            user.avatar = file
            db.session.commit()
        toonified_avatar_save = ToonifiedAvatar(
            user_id=user.id, image=toonified_avatar, uuid=uuid.uuid4()
        )
        avatar_uuid = toonified_avatar_save.uuid
        db.session.add(toonified_avatar_save)
        db.session.commit()
        db.session.close()
        """
        return ToonifiedAvatarOutput(image="aaa")


@strawberry.type
class Query:
    @strawberry.field
    def hello_world(self, info) -> str:
        return "hello world"


schema = strawberry.Schema(mutation=Mutation, query=Query)
